const users = angular.module("users", ["users.service"]);

angular.module("users").controller("UsersPageCtrl", ($scope, UsersService) => {
  const vm = ($scope.usersPage = {});

  vm.refresh = () => {
    UsersService.fetchUsers().then((data) => {
      vm.users = data;
    });
  };
  vm.refresh();

  vm.select = (id) => {
    return UsersService.fetchUserById(id).then((user) => {
      vm.selected = user;
      vm.mode = "details";
    });
  };

  vm.edit = () => {
    vm.mode = "edit";
    vm.draft = { ...vm.selected };
  };
  vm.save = (draft) => {
    UsersService.saveUpdatedUser(draft).then((saved) => {
      vm.select(saved.id);
      vm.refresh();
    });
  };
});
// .controller('UserListCtrl', ($scope) => {
//   const vm = $scope.list = {}
//   vm.filtered = []
//   vm.select = (id) => $scope.$emit('selectUser', id)

// })
// .controller('UserDetailsCtrl', ($scope) => {
//   const vm = $scope.details = {}
//   vm.user = null

//   $scope.$on('userSelected', (event, user) => { vm.user = (user) })

// })
// .controller('UserEditFormCtrl', ($scope) => {
//   const vm = $scope.editform = {}
//   vm.draft = {}
// })


// angular.module('config', [])
//   .constant('PAGES', [])

angular
  .module('myApp')
  .constant('PAGES', [
    { name: 'tasks', label: 'Tasks', tpl:'views/tasks-view.tpl.html' },
    { name: 'users', label: 'Users', tpl:'views/users-view.tpl.html' },
  ])
  .constant('INITIAL_TASKS', [{
    id: "123",
    name: "Task 123",
    completed: true
  }, {
    id: "234",
    name: "Task 234",
    completed: false
  }, {
    id: "345",
    name: "Task 345",
    completed: true
  }])
  

/// config phase... 
/// Configuration locks
/// run phase...
angular.bootstrap(document, ['myApp', 'tasks'])
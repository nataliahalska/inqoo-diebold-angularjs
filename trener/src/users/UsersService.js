
angular.module('users.service', [])
  .service('UsersService', function ($http) {

    this.fetchUsers = () => {
      return $http.get('http://localhost:3000/users')
        .then(res => res.data)
    }

    this.fetchUserById = (id) => {
      return $http.get(`http://localhost:3000/users/${id}`).then(resp => resp.data)
    }

    this.saveUpdatedUser = (user) => {
      return $http.put(`http://localhost:3000/users/${user.id}`, user).then(resp => resp.data)
    }

    this.saveNewUser = (user) => {
      return $http.post(`http://localhost:3000/users/`, user).then(resp => resp.data)
    }

    this.deleteUserById = (id) => {
      return $http.delete(`http://localhost:3000/users/${id}`).then(resp => resp.data)
    }
  });

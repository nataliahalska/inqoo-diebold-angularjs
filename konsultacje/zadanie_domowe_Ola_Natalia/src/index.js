angular.module("config", []);
angular.module("myApp", ["config"]);
const app = angular.module("myApp");
app.run(function ($rootScope) {
  $rootScope.title = "MyApp";
});

app.controller("AppCtrl", ($scope) => {
  const vm = ($scope.game = {});
  vm.title = "MyApp2";
  vm.user = { name: "Guest" };

  vm.board = [
    [null, null, null],
    [null, null, null],
    [null, null, null],
  ];

  vm.mode = "playerX";
  vm.status = "";
  vm.endOfGame = "";

  vm.makeMove = (row, column) => {
    if (vm.status === "") {
      if (vm.mode == "playerX" && vm.board[row][column] == null) {
        vm.board[row][column] = "x";

        vm.checkWinner("x");

        vm.mode = "playerO";
      } else if (vm.mode == "playerO" && vm.board[row][column] == null) {
        vm.board[row][column] = "O";
        vm.checkWinner("O");
        vm.mode = "playerX";
      }
    } else {
      console.log("ddd");
      vm.endOfGame =
        "End of game!";
    }
  }

    vm.checkWinner = (playerSign) => {
      if (
        (vm.board[0][0] == playerSign &&
          vm.board[1][1] == playerSign &&
          vm.board[2][2] == playerSign) ||
        (vm.board[0][0] == playerSign &&
          vm.board[0][1] == playerSign &&
          vm.board[0][2] == playerSign) ||
        (vm.board[1][0] == playerSign &&
          vm.board[1][1] == playerSign &&
          vm.board[1][2] == playerSign) ||
        (vm.board[2][0] == playerSign &&
          vm.board[2][1] == playerSign &&
          vm.board[2][2] == playerSign) ||
        (vm.board[0][0] == playerSign &&
          vm.board[1][0] == playerSign &&
          vm.board[2][0] == playerSign) ||
        (vm.board[0][1] == playerSign &&
          vm.board[1][1] == playerSign &&
          vm.board[2][1] == playerSign) ||
        (vm.board[0][2] == playerSign &&
          vm.board[1][2] == playerSign &&
          vm.board[2][2] == playerSign) ||
        (vm.board[2][0] == playerSign &&
          vm.board[1][1] == playerSign &&
          vm.board[0][2] == playerSign)
      ) {
        vm.status = "The winner is Player with Sign: " + playerSign;
      }
    };
  
    vm.newGame = () => {

      vm.board = [
        [null, null, null],
        [null, null, null],
        [null, null, null],
      ];
      vm.mode = "playerX";
  vm.status = "";
  vm.endOfGame = "";

    }


});
angular.bootstrap(document, ["myApp"]); // manual boostrap
